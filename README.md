# build-environment

[![license](https://img.shields.io/gitlab/license/build-environment/build-environment)](https://gitlab.com/build-environment/build-environment)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

This base docker image is useful to build compile and debug environment
docker images for developer and CI.
`build-environment` docker image has to be used with the
`run_docker.py` script.

The script runs interactively the docker image given as argument
while mounting the working directory and injecting the current user.
When used on linux, the working directory is mounted with the same path
as on the host.
The current user GID and PID are preserved inside the docker container.

## How-to build the docker image

To build the image, use the Makefile:

```shell
$ make build
...
```

## How-to launch the docker image

The `build-environment` docker image can be started using `run-docker.py` script:

```shell
$ ./run_docker.py -I registry.gitlab.com/build-environment/build-environment:latest \
> [... "docker run additional arguments"] \
> [-c ... "command executed in container"]
...
# OR
$ ./build-environment [... "docker run additional arguments"] \
> [-c ... "command executed in container"]
...
```

## How-to build a docker image based on build-environment image

Simply start your Dockerfile
`FROM registry.gitlab.com/build-environment/build-environment:latest`:

```Dockerfile
FROM registry.gitlab.com/build-environment/build-environment:latest

# ...

# if you want to set your own entrypoint, proceed like
COPY entrypoint.sh /post-entrypoint.sh
RUN chmod +x /post-entrypoint.sh
ENV ENTRYPOINT="/post-entrypoint.sh"
```

You may also provide a simple script to easily launch your image,
be inspired by `build-environment` script.
