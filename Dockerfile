# ------------------------------------------------------------------------------
# Pull base image
FROM ubuntu:22.04

# ------------------------------------------------------------------------------
# Install tools via apt
RUN export DEBIAN_FRONTEND=noninteractive \
    && set -x \
    && apt-get -y update \
    && apt-get -y upgrade \
    && apt-get -y install \
        wget \
        curl \
        gnupg \
        debian-archive-keyring \
        apt-transport-https \
        locales \
        vim \
        nano \
        sudo \
    && apt-get clean && rm -rf /var/lib/apt/lists

# ------------------------------------------------------------------------------
# Configure locale
RUN locale-gen en_US.UTF-8 && \
    update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# ------------------------------------------------------------------------------
# install gosu for a better su+exec command
# https://github.com/tianon/gosu
ARG GOSU_VERSION=1.10
RUN dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
    && chmod +xs /usr/local/bin/gosu \
    && gosu nobody true

# ------------------------------------------------------------------------------
# Set working directory
ARG WORKDIR=/root

# ------------------------------------------------------------------------------
# Copy and install new entrypoint
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]

ENV force_color_prompt=yes
CMD [ "/bin/bash" ]
