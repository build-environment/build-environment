#!/usr/bin/env bash

set -e

CMD=("$@")
if [[ -v ENTRYPOINT ]]; then
	CMD=("$ENTRYPOINT" "${CMD[@]}")
	unset ENTRYPOINT
fi

if [[ -n ${MP_USER:-""} ]]; then
	readonly M_USER=$MP_USER
	# Set UID and GID default value if not set
	readonly M_UID=${MP_UID:-1000}
	readonly M_GID=${MP_GID:-1000}
	readonly HOME_PATH=/home/$M_USER
	WORKDIR=$(realpath "$PWD")
	readonly WORKDIR

	# Unset the environment variables
	unset MP_USER
	unset MP_UID
	unset MP_GID

	if [[ -d $HOME_PATH ]]; then
		if [[ $HOME_PATH = "$WORKDIR" ]] || [[ $HOME_PATH = $WORKDIR/* ]]; then
			# HOME_PATH is a subdir of WORKDIR
			if [[ $(stat -c '%u' "$HOME_PATH") != "$M_UID" ]]; then
				echo "UID $M_UID is not owner of $HOME_PATH" 1>&2
				exit 1
			fi
		else
			# Set user owner of it's home
			chown "$M_UID":"$M_GID" "$HOME_PATH"
		fi
	fi

	# Create group or rename if exists
	if [[ -z $(getent group "$M_GID") ]]; then
		addgroup --gid "$M_GID" "$M_USER" >/dev/null
	else
		groupmod -n "$M_USER" "$(getent group "$M_GID" | cut -d: -f1)"
	fi

	# Create user or rename if exists
	if [[ -z $(getent passwd "$M_UID") ]]; then
		adduser --disabled-password --gecos "" --shell /bin/bash --home "$HOME_PATH" --uid "$M_UID" --gid "$M_GID" "$M_USER" >/dev/null
	else
		usermod --login "$M_USER" "$(id --name --user "$M_UID")"
		usermod --home "$HOME_PATH" "$M_USER"
	fi

	# Add user to sudoers
	echo "$M_USER ALL=(ALL) NOPASSWD: ALL" >"/etc/sudoers.d/$M_USER"

	# Copy files to home only
	# if HOME_PATH is not a subdir of WORKDIR
	# and WORKDIR != HOME_PATH
	if [[ $HOME_PATH/ != $WORKDIR/* ]] && [[ $WORKDIR != "$HOME_PATH" ]]; then
		# Copy files from current user home to HOME_PATH
		find ~/ -mindepth 1 -maxdepth 1 -type f -exec cp {} "$HOME_PATH" \;

		# Change HOME_PATH files owner
		find "$HOME_PATH" -mindepth 1 -maxdepth 1 -type f -uid "$(id --user root)" -exec chown "$M_UID":"$M_GID" {} \;
	fi

	# Execute the command with created user
	gosu "$M_USER" "${CMD[@]}"
else
	exec "${CMD[@]}"
fi
