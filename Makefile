DIR_PATH := $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
DOCKER_REGISTRY := registry.gitlab.com/build-environment
DOCKER_IMAGE_NAME = $(DOCKER_REGISTRY)/build-environment
BUILD_DIR ?= $(DIR_PATH)/build
DEPS = Makefile Dockerfile entrypoint.sh

$(BUILD_DIR):
	@mkdir -p $@
	@echo '*' > $@/.gitignore

$(BUILD_DIR)/$(DOCKER_IMAGE_NAME): $(BUILD_DIR) $(addprefix $(DIR_PATH)/, $(DEPS))
	@mkdir -p $(dir $@)
	@docker build --tag $(DOCKER_IMAGE_NAME) "$(DIR_PATH)" --progress=plain 2>&1 | tee "$@.log"
	@docker images "$(DOCKER_IMAGE_NAME)" --no-trunc --quiet > $@

build: $(BUILD_DIR)/$(DOCKER_IMAGE_NAME)

deploy: build
	docker push $(DOCKER_IMAGE_NAME)

clean:
	docker rmi $(DOCKER_IMAGE_NAME)
	rm -rf $(BUILD_DIR)

all: build

.PHONY : all build deploy clean
.DEFAULT_GOAL := all
